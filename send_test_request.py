import json

URL = 'http://luxwinclub-stg.bizico.com:8080/casino-deploy'
HEADERS= {'Content-type': 'application/json', 'Accept': 'text/plain'}

data = \
{
    u'push': {
        u'changes': [{
            u'forced': False,
            u'old': None,
            u'links': {
                u'commits': {
                    u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commits?include=f5f1072d086a9a659f21109184de37d218f30999'
                },
                u'html': {
                    u'href': u'https://bitbucket.org/AndyBezh/hackndev/branch/develop'
                }
            },
            u'created': True,
            u'commits': [{
                u'hash': u'f5f1072d086a9a659f21109184de37d218f30999',
                u'links': {
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/f5f1072d086a9a659f21109184de37d218f30999'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/f5f1072d086a9a659f21109184de37d218f30999'
                    }
                },
                u'author': {
                    u'raw': u'AndyBezh <andrewbox08@gmail.com>',
                    u'user': {
                        u'username': u'AndyBezh',
                        u'type': u'user',
                        u'display_name': u'AndyBezh',
                        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/'
                            },
                            u'avatar': {
                                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                            }
                        }
                    }
                },
                u'parents': [{
                    u'hash': u'f19c3ced2567e7907f225dd88fe4f8f617b4a85d',
                    u'type': u'commit',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                        }
                    }
                }],
                u'date': u'2016-02-10T12:16:36+00:00',
                u'message': u'develop init\n',
                u'type': u'commit'
            }, {
                u'hash': u'f19c3ced2567e7907f225dd88fe4f8f617b4a85d',
                u'links': {
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                    }
                },
                u'author': {
                    u'raw': u'AndyBezh <andrewbox08@gmail.com>',
                    u'user': {
                        u'username': u'AndyBezh',
                        u'type': u'user',
                        u'display_name': u'AndyBezh',
                        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/'
                            },
                            u'avatar': {
                                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                            }
                        }
                    }
                },
                u'parents': [{
                    u'hash': u'073f0377ac385caa1e47dd87a5425405f4a1ec63',
                    u'type': u'commit',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/073f0377ac385caa1e47dd87a5425405f4a1ec63'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/073f0377ac385caa1e47dd87a5425405f4a1ec63'
                        }
                    }
                }],
                u'date': u'2016-02-10T10:06:35+00:00',
                u'message': u'test changed\n',
                u'type': u'commit'
            }, {
                u'hash': u'073f0377ac385caa1e47dd87a5425405f4a1ec63',
                u'links': {
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/073f0377ac385caa1e47dd87a5425405f4a1ec63'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/073f0377ac385caa1e47dd87a5425405f4a1ec63'
                    }
                },
                u'author': {
                    u'raw': u'Andrew Bezhenar <andrewbox08@gmail.com>',
                    u'user': {
                        u'username': u'AndyBezh',
                        u'type': u'user',
                        u'display_name': u'AndyBezh',
                        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/'
                            },
                            u'avatar': {
                                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                            }
                        }
                    }
                },
                u'parents': [{
                    u'hash': u'974b27ef44a457263a160c80c61f280311987af8',
                    u'type': u'commit',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/974b27ef44a457263a160c80c61f280311987af8'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/974b27ef44a457263a160c80c61f280311987af8'
                        }
                    }
                }],
                u'date': u'2016-02-08T10:32:49+00:00',
                u'message': u'test branch commit\n',
                u'type': u'commit'
            }, {
                u'hash': u'974b27ef44a457263a160c80c61f280311987af8',
                u'links': {
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/974b27ef44a457263a160c80c61f280311987af8'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/974b27ef44a457263a160c80c61f280311987af8'
                    }
                },
                u'author': {
                    u'raw': u'Andrew Bezhenar <andrewbox08@gmail.com>',
                    u'user': {
                        u'username': u'AndyBezh',
                        u'type': u'user',
                        u'display_name': u'AndyBezh',
                        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/'
                            },
                            u'avatar': {
                                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                            }
                        }
                    }
                },
                u'parents': [{
                    u'hash': u'9b1f67cf8fc0a1b9e248074912df21a784a7b7cb',
                    u'type': u'commit',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/9b1f67cf8fc0a1b9e248074912df21a784a7b7cb'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/9b1f67cf8fc0a1b9e248074912df21a784a7b7cb'
                        }
                    }
                }],
                u'date': u'2016-02-08T10:32:11+00:00',
                u'message': u'test 1\n',
                u'type': u'commit'
            }, {
                u'hash': u'9b1f67cf8fc0a1b9e248074912df21a784a7b7cb',
                u'links': {
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/9b1f67cf8fc0a1b9e248074912df21a784a7b7cb'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/9b1f67cf8fc0a1b9e248074912df21a784a7b7cb'
                    }
                },
                u'author': {
                    u'raw': u'AndyBezh <andrewbox08@gmail.com>',
                    u'user': {
                        u'username': u'AndyBezh',
                        u'type': u'user',
                        u'display_name': u'AndyBezh',
                        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/'
                            },
                            u'avatar': {
                                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                            }
                        }
                    }
                },
                u'parents': [{
                    u'hash': u'4b04084ea142f297cda847955bad4ad904d41a70',
                    u'type': u'commit',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/4b04084ea142f297cda847955bad4ad904d41a70'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/4b04084ea142f297cda847955bad4ad904d41a70'
                        }
                    }
                }],
                u'date': u'2016-02-05T16:03:57+00:00',
                u'message': u'webhook test 4\n',
                u'type': u'commit'
            }],
            u'truncated': True,
            u'closed': False,
            u'new': {
                u'repository': {
                    u'uuid': u'{0adfa4bf-5a37-4cf1-949c-379c03e45373}',
                    u'type': u'repository',
                    u'full_name': u'AndyBezh/hackndev',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev'
                        },
                        u'avatar': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/avatar/32/'
                        }
                    },
                    u'name': u'hackndev'
                },
                u'type': u'branch',
                u'target': {
                    u'hash': u'f5f1072d086a9a659f21109184de37d218f30999',
                    u'links': {
                        u'self': {
                            u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/f5f1072d086a9a659f21109184de37d218f30999'
                        },
                        u'html': {
                            u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/f5f1072d086a9a659f21109184de37d218f30999'
                        }
                    },
                    u'author': {
                        u'raw': u'AndyBezh <andrewbox08@gmail.com>',
                        u'user': {
                            u'username': u'AndyBezh',
                            u'type': u'user',
                            u'display_name': u'AndyBezh',
                            u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
                            u'links': {
                                u'self': {
                                    u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                                },
                                u'html': {
                                    u'href': u'https://bitbucket.org/AndyBezh/'
                                },
                                u'avatar': {
                                    u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                                }
                            }
                        }
                    },
                    u'parents': [{
                        u'hash': u'f19c3ced2567e7907f225dd88fe4f8f617b4a85d',
                        u'type': u'commit',
                        u'links': {
                            u'self': {
                                u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commit/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                            },
                            u'html': {
                                u'href': u'https://bitbucket.org/AndyBezh/hackndev/commits/f19c3ced2567e7907f225dd88fe4f8f617b4a85d'
                            }
                        }
                    }],
                    u'date': u'2016-02-10T12:16:36+00:00',
                    u'message': u'develop init\n',
                    u'type': u'commit'
                },
                u'links': {
                    u'commits': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/commits/develop'
                    },
                    u'self': {
                        u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev/refs/branches/develop'
                    },
                    u'html': {
                        u'href': u'https://bitbucket.org/AndyBezh/hackndev/branch/develop'
                    }
                },
                u'name': u'develop'
            }
        }]
    }, u'actor': {
        u'username': u'AndyBezh',
        u'type': u'user',
        u'display_name': u'AndyBezh',
        u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
        u'links': {
            u'self': {
                u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
            },
            u'html': {
                u'href': u'https://bitbucket.org/AndyBezh/'
            },
            u'avatar': {
                u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
            }
        }
    }, u'repository': {
        u'website': u'',
        u'scm': u'git',
        u'uuid': u'{0adfa4bf-5a37-4cf1-949c-379c03e45373}',
        u'links': {
            u'self': {
                u'href': u'https://api.bitbucket.org/2.0/repositories/AndyBezh/hackndev'
            },
            u'html': {
                u'href': u'https://bitbucket.org/AndyBezh/hackndev'
            },
            u'avatar': {
                u'href': u'https://bitbucket.org/AndyBezh/hackndev/avatar/32/'
            }
        },
        u'full_name': u'AndyBezh/hackndev',
        u'owner': {
            u'username': u'AndyBezh',
            u'type': u'user',
            u'display_name': u'AndyBezh',
            u'uuid': u'{0c6bea9c-3add-4cef-a697-21f3e85b6dbe}',
            u'links': {
                u'self': {
                    u'href': u'https://api.bitbucket.org/2.0/users/AndyBezh'
                },
                u'html': {
                    u'href': u'https://bitbucket.org/AndyBezh/'
                },
                u'avatar': {
                    u'href': u'https://bitbucket.org/account/AndyBezh/avatar/32/'
                }
            }
        },
        u'type': u'repository',
        u'is_private': True,
        u'name': u'hackndev'
    }
}

import requests, json

r = requests.post(URL, data=json.dumps(data), headers=HEADERS)
print r