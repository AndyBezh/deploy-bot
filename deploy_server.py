#!/usr/bin/env python

from bottle import post, request, run
from deploy_task import deploy_task
import ijson
import logging

logging.basicConfig(
    filename='deploy.log',
    filemode='w',
    format='%(asctime)s %(name)s %(levelname)s: %(message)s',
    level=logging.DEBUG
)

log = logging.getLogger(__name__)

HOST = '0.0.0.0'
PORT = 8080
DEBUG = True


log.info('deploy server start at %s:%s\n%s', HOST, PORT, '-'*40)

@post('/casino-deploy')
def deploy_server():
    try:
        changes = ijson.items(request.body, 'push.changes')
        changes = [c for c in changes]
    except Exception, e:
        log.error('Parsing push.changes name in json failed!', exc_info=True)
        return
    
    if not changes:
        log.warn('No changes in POST!')
        return

    if len(changes) > 1:
        log.warn('Changes more then one instance in array!')
        log.debug([c for c in changes])

    for c in changes:
        try:
            branch_name = [i['new']['name'] for i in c][0]
        except Exception, e:
            log.error('Parsing branch name in json failed!', exc_info=True)
            raise e
        try:
            log.info('Starting deploy task for branch %s...', branch_name)
            deploy_task.apply_async(args=[branch_name])
        except Exception, e:
            log.error(
                'Deploy task for branch %s failed to start!', branch_name,
                exc_info=True
            )
            raise e
    return 

run(host=HOST, port=PORT, debug=DEBUG)
