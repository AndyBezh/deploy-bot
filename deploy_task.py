from subprocess import check_call, CalledProcessError
from celery.utils.log import get_task_logger
from celery import Celery
import os

CELERYD_LOG_FILE="deploy_worker.log"
deploy_worker = Celery('bot', broker='redis://localhost:6379/1')

log = get_task_logger(__name__)
log.handlers
@deploy_worker.task
def deploy_task(branch_name):
    def _clean(test_prj_dir, test_ven_dir):
        check_call(
            "rm -rf {test_prj_dir} {test_ven_dir} " \
            .format(test_prj_dir=test_prj_dir, test_ven_dir=test_ven_dir), \
            shell=True
        )
        log.info('Test project files %s and %s removed', test_prj_dir, test_ven_dir)


    log.debug('deploy task start\n%s\n', '-'*40)
    if str(branch_name) == 'develop':
        WORK_PRJ_DIR = '/home/lwc_dev/luxwinclub'
        TEST_PRJ_DIR = '/home/lwc_dev/luxwinclub_test'

        WORK_VENV_DIR = '/home/lwc_dev/.virtualenvs/lwc_dev'
        TEST_VENV_DIR = '/home/lwc_dev/.virtualenvs/lwc_dev_test'

        if not os.path.exists(TEST_PRJ_DIR):
            try:
                log.info('Cloning project for branch %s...', branch_name)
                check_call(
                    "sudo -H -iu lwc_dev " \
                    "bash -ic 'cp -r {work_dir} {test_dir} " \
                    .format(work_dir=WORK_PRJ_DIR, test_dir=TEST_PRJ_DIR) \
                    + \
                    "&& virtualenv-clone {work_venv_dir} {test_venv_dir} " \
                    .format(work_venv_dir=WORK_VENV_DIR, test_venv_dir=TEST_VENV_DIR) \
                    + \
                    "&& workon lwc_dev_test && cd {test_dir}/casino && setvirtualenvproject'" \
                    .format(test_dir=TEST_PRJ_DIR), \
                    shell=True
                )
                log.info('Clone project Success for branch %s!', branch_name)
            except CalledProcessError:
                test_passed = False
                log.error('Clone project Failed for branch %s', branch_name, exc_info=True)
                _clean(TEST_PRJ_DIR, TEST_VENV_DIR)
                raise CalledProcessError
        try:
            log.info('Building project for branch %s... ', branch_name)
            check_call(
                "sudo -H -iu lwc_dev " \
                "bash -ic 'workon lwc_dev_test " \
                "&& git pull "
                "&& pip install -r requirements.txt " \
                "&& npm install && bower install'",
                shell=True
            )
            log.info("Build Success for branch %s!", branch_name)
        except CalledProcessError:
            test_passed = False
            log.error('Build Failed for branch %s', branch_name, exc_info=True)
            _clean(TEST_PRJ_DIR, TEST_VENV_DIR)
            raise CalledProcessError

        try:
            log.info("Testing branch %s...", branch_name)
            check_call(
                "sudo -H -iu lwc_dev " \
                "bash -ic 'workon lwc_dev_test " \
                "&& coverage run manage.py test'",
                shell=True
            )
            log.info("Tests Success for branch %s!", branch_name)
            test_passed = True
        except CalledProcessError:
            test_passed = False
            log.error('Tests Failed for branch %s', branch_name, exc_info=True)
            _clean(TEST_PRJ_DIR, TEST_VENV_DIR)
            raise CalledProcessError

        if test_passed:
            log.info("Tests passed, Deploying branch...%s", branch_name)
            check_call(
                "sudo -H -iu lwc_dev " \
                "bash -ic '/home/lwc_dev/luxwinclub/update.sh' &&" \
                "supervisorctl restart dev:*",
                shell=True
            )
            log.info('Deploy Success for branch %s!\n%s\n', branch_name, '-'*40)

    log.debug('deploy task end\n%s\n', '-'*40)